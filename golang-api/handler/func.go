package handler

import (
	"fmt"
	game "golang-api/game"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
)

type gameHandler struct {
	gameService game.Service
}

func NewGameHander(gameService game.Service) *gameHandler {
	return &gameHandler{gameService}
}

// Root GET Handler show a JSON Welcome Message
func (handler gameHandler) RootHandler(c *gin.Context) {
	welcomeMsg := fmt.Sprintf("Welcome to my Database!")
	c.JSON(http.StatusOK, welcomeMsg)
}

// CRUD Endpoints Handler
func (handler gameHandler) GetGamesHandler(c *gin.Context) {
	games, err := handler.gameService.FindAll()
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": err,
		})
		return
	}

	var gamesResponse []game.GameResponse

	for _, g := range games {
		gameResponse := convertResponse(g)
		gamesResponse = append(gamesResponse, gameResponse)
	}

	c.JSON(http.StatusOK, gin.H{
		"data": gamesResponse,
	})
}

// Path Variable Handler
func (handler gameHandler) PathHandler(c *gin.Context) {
	idString := c.Param("id")
	id, _ := strconv.Atoi(idString)

	g, err := handler.gameService.FindByID(id)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": err,
		})
		return
	}

	gameResponse := convertResponse(g)

	c.JSON(http.StatusOK, gin.H{
		"data": gameResponse,
	})
}

// Query String Handler
func (handler gameHandler) QueryHandler(c *gin.Context) {
	title := c.Query("title")
	year := c.Query("year")

	c.JSON(http.StatusOK, gin.H{"title": title, "year": year})
}

// PUT / Update a Game
func (handler gameHandler) UpdateGameHandler(c *gin.Context) {
	var gameRequest game.GameRequest

	err := c.ShouldBindJSON(&gameRequest)
	if err != nil {
		//Error Validation
		errorMsgs := []string{}
		//log.Fatal(err) will cause program & server to terminate
		for _, e := range err.(validator.ValidationErrors) {
			errorMsg := fmt.Sprintf("Error on field %s, condition: %s", e.Field(), e.ActualTag())

			errorMsgs = append(errorMsgs, errorMsg)
		}
		c.JSON(http.StatusBadRequest, errorMsgs)
		fmt.Println(err)
		return
	}

	idString := c.Param("id")
	id, _ := strconv.Atoi(idString)

	game, err := handler.gameService.Update(id, gameRequest)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": err,
		})
	}

	c.JSON(http.StatusOK, gin.H{
		"data": game,
	})
}

// POST / Create a Game
func (handler gameHandler) PostGamesHandler(c *gin.Context) {
	var gameRequest game.GameRequest

	err := c.ShouldBindJSON(&gameRequest)
	if err != nil {
		//Error Validation
		errorMsgs := []string{}
		//log.Fatal(err) will cause program & server to terminate
		for _, e := range err.(validator.ValidationErrors) {
			errorMsg := fmt.Sprintf("Error on field %s, condition: %s", e.Field(), e.ActualTag())

			errorMsgs = append(errorMsgs, errorMsg)
		}
		c.JSON(http.StatusBadRequest, errorMsgs)
		fmt.Println(err)
		return
	}

	game, err := handler.gameService.Create(gameRequest)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": err,
		})
	}

	//Outputs data to response request
	c.JSON(http.StatusOK, gin.H{
		"data": game,
	})
}

// DELETE Handler
func (handler gameHandler) DeleteGameHandler(c *gin.Context) {
	var gameRequest game.GameRequest
	idString := c.Param("id")
	id, _ := strconv.Atoi(idString)

	game, err := handler.gameService.Delete(id, gameRequest)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": err,
		})
	}

	c.JSON(http.StatusOK, gin.H{
		"data": game,
	})
}

func convertResponse(gameObj game.Game) game.GameResponse {
	return game.GameResponse{
		ID:    gameObj.ID,
		Title: gameObj.Title,
		Year:  gameObj.Year,
		Genre: gameObj.Genre,
	}
}
