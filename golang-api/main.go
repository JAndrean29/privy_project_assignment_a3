package main

import (
	"fmt"
	game "golang-api/game"
	handler "golang-api/handler"
	"log"

	"github.com/gin-gonic/gin"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func main() {
	router := gin.Default()

	//API Versioning
	v1 := router.Group("/v1")

	//initate connection to mySql db
	dsn := "root:@tcp(127.0.0.1:3306)/golang-api?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatal("Connection failed")
	}

	fmt.Println("Connected to Database")
	//migrate data pada struct untuk dijadikan tabel pada mysql
	db.AutoMigrate(&game.Game{})

	gameRepository := game.NewRepository(db)
	//gameFileRepository := game.NewFileRepository() //Local File Handler
	gameService := game.NewService(gameRepository)
	gameHandler := handler.NewGameHander(gameService)

	//Endpoints and their handler function
	v1.GET("/", gameHandler.RootHandler)
	v1.GET("/games", gameHandler.GetGamesHandler)
	v1.POST("/games", gameHandler.PostGamesHandler)
	v1.GET("/games/:id", gameHandler.PathHandler)
	v1.PUT("/games/:id", gameHandler.UpdateGameHandler)
	v1.DELETE("/games/:id", gameHandler.DeleteGameHandler)

	router.Run()
}
