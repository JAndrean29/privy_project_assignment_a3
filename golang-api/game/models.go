package game

import (
	"time"
)

// Struct and Data Binding
type Game struct {
	ID           int    //`json:"id" bson:"_id"`
	Title        string //`json:"title" binding:"required" bson:"title"` //makes title requires a value in it
	Year         int    //`json:"year" binding:"required,number" bson:"year"`
	Genre        string //`json:"genre" bson:"genre"`
	AddedAt      time.Time
	LastModified time.Time
}
