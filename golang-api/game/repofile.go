package game

import (
	"errors"
	"fmt"
)

type fileRepository struct {
}

func NewFileRepository() *fileRepository {
	return &fileRepository{}
}

func (r *fileRepository) FindAll() ([]Game, error) {
	var games []Game

	fmt.Println("Finding all games")

	return games, errors.New("Success")
}

func (r *fileRepository) FindByID(ID int) (Game, error) {
	var game Game

	fmt.Println("Finding requested game")

	return game, errors.New("Success")
}

func (r *fileRepository) Create(game Game) (Game, error) {

	fmt.Println("Game Added")

	return game, errors.New("Success")
}
