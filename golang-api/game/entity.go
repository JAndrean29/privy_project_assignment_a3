package game

import "gorm.io/gorm"

type Repository interface {
	FindAll() ([]Game, error)
	FindByID(ID int) (Game, error)
	Create(game Game) (Game, error)
	Update(game Game) (Game, error)
	Delete(game Game) (Game, error)
}

type repository struct {
	db *gorm.DB
}

func NewRepository(db *gorm.DB) *repository {
	return &repository{db}
}

func (r *repository) FindAll() ([]Game, error) {
	var games []Game

	err := r.db.Find(&games).Error

	return games, err
}

func (r *repository) FindByID(ID int) (Game, error) {
	var game Game

	err := r.db.Find(&game, ID).Error

	return game, err
}

func (r *repository) Create(game Game) (Game, error) {
	err := r.db.Create(&game).Error

	return game, err
}

func (r *repository) Update(game Game) (Game, error) {
	err := r.db.Save(&game).Error

	return game, err
}

func (r *repository) Delete(game Game) (Game, error) {
	err := r.db.Delete(&game).Error

	return game, err
}
