package game

type Service interface {
	FindAll() ([]Game, error)
	FindByID(ID int) (Game, error)
	Create(game GameRequest) (Game, error)
	Update(ID int, game GameRequest) (Game, error)
	Delete(ID int, game GameRequest) (Game, error)
}

type service struct {
	repository Repository
}

func NewService(repository Repository) *service {
	return &service{repository}
}

func (s *service) FindAll() ([]Game, error) {
	games, err := s.repository.FindAll()
	return games, err
}

func (s *service) FindByID(ID int) (Game, error) {
	game, err := s.repository.FindByID(ID)
	return game, err
}

func (s *service) Create(gameRequest GameRequest) (Game, error) {

	game := Game{
		Title: gameRequest.Title,
		Year:  gameRequest.Year,
		Genre: gameRequest.Genre,
	}

	newGame, err := s.repository.Create(game)
	return newGame, err
}

func (s *service) Update(ID int, gameRequest GameRequest) (Game, error) {
	game, err := s.repository.FindByID(ID)

	game.Title = gameRequest.Title
	game.Year = gameRequest.Year
	game.Genre = gameRequest.Genre

	newGame, err := s.repository.Update(game)
	return newGame, err
}

func (s *service) Delete(ID int, gameRequest GameRequest) (Game, error) {
	game, err := s.repository.FindByID(ID)

	gameDlt, err := s.repository.Delete(game)

	return gameDlt, err
}
